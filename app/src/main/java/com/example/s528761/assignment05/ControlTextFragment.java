package com.example.s528761.assignment05;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ControlTextFragment extends Fragment {


    public ControlTextFragment() {
        // Required empty public constructor
    }
    public interface TextControl{

        public void updateString(String rep);
    }
    private TextControl textActivity;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        textActivity=(TextControl) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_control_text, container, false);

        Button btn = (Button) v.findViewById(R.id.firstbtn);
        final EditText update = (EditText)v.findViewById(R.id.updateTextET);
        btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(update.getText().length()!=0) {
                    textActivity.updateString(update.getText().toString());
                    update.setText("");
                }else{
                    Toast.makeText(getContext(),"Enter text to update the TextView",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

}
