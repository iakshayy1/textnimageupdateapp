package com.example.s528761.assignment05;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageTextFragment extends Fragment {


    public ImageTextFragment() {
        // Required empty public constructor
    }
    public interface ImageControl{
        public void updateImage(String rep);
    }
    private ImageControl imageActivity;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        imageActivity = (ImageControl)context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_image_text, container, false);
        Button btn = (Button) v.findViewById(R.id.secndBt);
        final EditText update = (EditText)v.findViewById(R.id.updateImageET);
        btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                   imageActivity.updateImage(update.getText().toString());
                    update.setText("");

            }
        });


        return v;
    }

}
