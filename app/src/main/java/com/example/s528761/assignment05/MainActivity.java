package com.example.s528761.assignment05;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ControlTextFragment.TextControl, ImageTextFragment.ImageControl, ThirdFragment.ThirdFR {
    private ConstraintLayout mConstraintLayout;
    private Fragment first;
    private Fragment second;
    private Fragment third;
    private boolean firstShowing;
    private boolean secondShowing;
    private boolean doAdd;
    private boolean addOther;
    ImageView image;
    TextView text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mConstraintLayout = (ConstraintLayout)findViewById(R.id.constraintLayout);
        image = (ImageView) findViewById(R.id.myImage);
        text = (TextView) findViewById(R.id.myText);
        first = new ControlTextFragment();
        second = new ImageTextFragment();
        third = new ThirdFragment();


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction trans = fm.beginTransaction();
        trans.add(R.id.fragmentLocation, first);
        firstShowing = true;
        secondShowing = true;
        doAdd = true;
        addOther = true;
        trans.commit();
        Button swapper = (Button) findViewById(R.id.swap);
        swapper.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(firstShowing){
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.detach(first);
                    if(doAdd) {
                        trans.add(R.id.fragmentLocation, second);
                    }else{
                        trans.attach(second);
                    }
                    doAdd=false;
                    firstShowing = false;
                    secondShowing = true;
                    trans.commit();

                }else if(secondShowing){
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.detach(second);
                    if(addOther) {
                        trans.add(R.id.fragmentLocation, third);
                    }else{
                        trans.attach(third);
                    }
                    addOther=false;
                    secondShowing = false;
                    trans.commit();

                }else{
                    FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                    trans.detach(third);
                    trans.attach(first);
                    firstShowing = true;
                    trans.commit();
                }
            }
        });

    }
   @Override
    public void updateString(String rep){

       text.setText(rep);
            }
    @Override
    public void updateImage(String rep){
      if(rep.equals("animal")){
          image.setImageResource(R.drawable.dog);

        }else if(rep.equalsIgnoreCase("flower")){
          image.setImageResource(R.drawable.flower);

        }else if(rep.equalsIgnoreCase("snow")) {
          image.setImageResource(R.drawable.snow);

      } else if(rep.equalsIgnoreCase("nature")) {
          image.setImageResource(R.drawable.nature);

      }else if(rep.equalsIgnoreCase("wildlife")) {
          image.setImageResource(R.drawable.wildlife);

      }else if(rep.equalsIgnoreCase("waterfalls")) {
          image.setImageResource(R.drawable.waterfalls);

      }else if(rep.equalsIgnoreCase("map")) {
          image.setImageResource(R.drawable.map);

      }else if(rep.equalsIgnoreCase("bridge")) {
          image.setImageResource(R.drawable.bridge);

      }else{
          image.setImageResource(R.drawable.question);

        }

    }
    @Override
    public void swapOut(){
               mConstraintLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
    }
    @Override
    public void resize(){
        image.setPadding(2,2,2,2);
        Toast.makeText(getApplicationContext(),"Image Resized",Toast.LENGTH_SHORT).show();
    }

}
