package com.example.s528761.assignment05;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdFragment extends Fragment {
    private int count=0;
    private int count1=0;

    public ThirdFragment() {
        // Required empty public constructor
    }
    public interface ThirdFR{
        public void swapOut();
        public void resize();
    }
    private ThirdFR lastActivity;
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        lastActivity = (ThirdFR) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_third, container, false);
        Button bt = (Button) v.findViewById(R.id.color);
        Button bt1 = (Button) v.findViewById(R.id.resize);
        bt.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
               lastActivity.swapOut();
            }
        });
        bt1.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v){
                lastActivity.resize();
            }
        });

        return v;
    }

}
